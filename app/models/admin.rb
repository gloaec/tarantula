=begin rdoc

Admin specific user logic.

=end
class Admin < User # STI
  def admin?; true; end
  
  # just create all missing project assignments with group 'ADMIN'
  # N.B. doesn't change the behaviour of Project#assignments
  def project_assignments_with_admin
    missing = Project.all - self.projects
    
    missing.each do |m|
      ProjectAssignment.create!(:user_id => self.id, :project_id => m.id, 
                                :group => 'ADMIN')
    end
    # NOTE: Undocumented + gone private
    self.send(:clear_association_cache) unless missing.empty?
    project_assignments_without_admin
  end
  # NOTE: Removed alias_method_chain
  #alias_method_chain :project_assignments, :admin
  alias_method :project_assignments_without_admin, :project_assignments
  alias_method :project_assignments, :project_assignments_with_admin
  
  def allowed_in_project?(pid,req_groups = nil); true; end

  def remove_admin_assignments
    admin_assignments = ProjectAssignment.where(:user_id => self.id, 
                                                :group => 'ADMIN')
    admin_assignments.map(&:destroy)
  end
  
end
