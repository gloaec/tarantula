class AddDeletedToRequirements < ActiveRecord::Migration[4.2]
  def self.up
    add_column :requirements, :deleted, :boolean, :default => false
  end

  def self.down
    remove_column :requirements, :deleted
  end
end
