class AddLibraryAttributeToProjects < ActiveRecord::Migration[4.2]
  def self.up
    add_column :projects, :library, :boolean, :default => false
  end

  def self.down
    remove_column :projects, :library
  end
  
end
