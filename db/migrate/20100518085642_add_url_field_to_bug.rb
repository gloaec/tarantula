class AddUrlFieldToBug < ActiveRecord::Migration[4.2]
  def self.up
    add_column :bugs, :url, :string
  end

  def self.down
    remove_column :bugs, :url
  end
end
