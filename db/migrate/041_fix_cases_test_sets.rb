class FixCasesTestSets < ActiveRecord::Migration[4.2]
  def self.up
    cases_test_sets = CasesTestSet.all.order('test_set_version DESC')
    cases_test_sets.each{|c|
      cases_test_sets.delete(c)
      cases_test_sets.each{|t|
        if ((c.case_id == t.case_id) && (c.test_set_id == t.test_set_id))
          t.destroy
        end
      }
    }
  rescue
    puts "WARNING: model CasesTestSets does not exist anymore"
  end

  def self.down
  end
end
