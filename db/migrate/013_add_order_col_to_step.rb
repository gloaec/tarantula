class AddOrderColToStep < ActiveRecord::Migration[4.2]
  def self.up
    add_column :steps, :order, :integer
  end

  def self.down
    remove_column :steps, :order
  end
end
