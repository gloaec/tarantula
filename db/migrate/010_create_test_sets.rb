class CreateTestSets < ActiveRecord::Migration[4.2]
  def self.up
    create_table :test_sets do |t|
      t.column :name, :string
    end
  end

  def self.down
    drop_table :test_sets
  end
end
