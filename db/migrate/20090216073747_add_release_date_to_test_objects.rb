class AddReleaseDateToTestObjects < ActiveRecord::Migration[4.2]
  def self.up
    add_column :test_objects, :release_date, :date
  end

  def self.down
    remove_column :test_objects, :release_date
  end
end
