class AddTestObjectForExecution < ActiveRecord::Migration[4.2]
  def self.up
    add_column :executions, :test_object, :string, :default => 'unknown'
  end

  def self.down
    remove_column :executions, :test_object
  end
end
