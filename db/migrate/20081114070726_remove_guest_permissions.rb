class RemoveGuestPermissions < ActiveRecord::Migration[4.2]
  def self.up
    ProjectAssignment.where(:group => 'GUEST').destroy_all
  end

  def self.down
  end
end
