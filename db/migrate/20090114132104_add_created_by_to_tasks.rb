class AddCreatedByToTasks < ActiveRecord::Migration[4.2]
  def self.up
    add_column :tasks, :created_by, :integer
  end

  def self.down
    remove_column :tasks, :created_by
  end
end
