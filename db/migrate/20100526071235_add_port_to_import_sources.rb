class AddPortToImportSources < ActiveRecord::Migration[4.2]
  def self.up
    add_column :import_sources, :port, :integer
  end

  def self.down
    remove_column :import_sources, :port
  end
end
