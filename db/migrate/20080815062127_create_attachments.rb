class CreateAttachments < ActiveRecord::Migration[4.2]
  def self.up
    create_table :attachments do |t|
      t.string :orig_filename
      t.timestamps
    end
  end
  
  def self.down
    drop_table :attachments
  end
end
