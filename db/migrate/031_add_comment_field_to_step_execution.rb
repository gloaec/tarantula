class AddCommentFieldToStepExecution < ActiveRecord::Migration[4.2]
  def self.up
    add_column :step_executions, :comment, :text
  end

  def self.down
    remove_column :step_executions, :comment
  end
end
