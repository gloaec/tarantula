class AddCaseIdToStepExecutions < ActiveRecord::Migration[4.2]
  def self.up
    add_column :step_executions, :case_id, :integer
  end

  def self.down
    remove_column :step_executions, :case_id
  end
end
