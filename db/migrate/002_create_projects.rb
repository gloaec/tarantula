class CreateProjects < ActiveRecord::Migration[4.2]
  def self.up
    create_table :projects do |t|
      t.column :name, :string
      t.column :description, :text
      t.column :visible, :boolean
    end
  end

  def self.down
    drop_table :projects
  end
end
