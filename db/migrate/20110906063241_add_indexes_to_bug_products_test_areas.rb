class AddIndexesToBugProductsTestAreas < ActiveRecord::Migration[4.2]
  def self.up
    add_index :bug_products_test_areas, [:bug_product_id, :test_area_id], name: :bug_product_test_area_idx
  end

  def self.down
    remove_index :bug_products_test_areas, [:bug_product_id, :test_area_id], name: :bug_product_test_area_idx
  end
end
