class AddResultToExecution < ActiveRecord::Migration[4.2]
  def self.up
    add_column :executions, :result, :string, :default => 'NOT_RUN'
  end

  def self.down
    remove_column :executions, :result
  end
end
