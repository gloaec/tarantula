class RemoveDefectFieldFromStepExecutions < ActiveRecord::Migration[4.2]
  
  def self.up
    # do some data migration here for defect texts?
    remove_column :step_executions, :defect
  end
  
  def self.down
    add_column :step_executions, :defect, :text
  end
  
end
