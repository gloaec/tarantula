class AddLatestProjectFieldToUser < ActiveRecord::Migration[4.2]
  def self.up
    add_column :users, :latest_project_id, :integer
  end

  def self.down
    remove_column :users, :latest_project_id
  end
end
