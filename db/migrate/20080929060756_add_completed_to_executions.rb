class AddCompletedToExecutions < ActiveRecord::Migration[4.2]
  
  def self.up
    add_column :executions, :completed, :boolean, :default => false
  end

  def self.down
    remove_column :executions, :completed
  end
  
end
