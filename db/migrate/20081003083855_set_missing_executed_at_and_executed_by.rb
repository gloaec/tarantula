class SetMissingExecutedAtAndExecutedBy < ActiveRecord::Migration[4.2]
  def self.up
    ces = CaseExecution.where(
      :result => ['PASSED', 'SKIPPED', 'FAILED'],
      :executed_at => nil
    )
    # NOTE: No data yet
    #u = User.find(1)
    ces.each do |ce|
      executor = ce.executed_by || u
      ce.update_attributes!(:executed_at => 3.months.ago,
                            :executed_by => executor)
    end
  end

  def self.down
  end
end
