class RemoveCategoryIdFromCases < ActiveRecord::Migration[4.2]
  def self.up
    if Case.columns.detect {|c| c.name == 'case_category_id'}
      begin
        remove_column :cases, :case_category_id if column_exists? :cases, :case_category_id
        remove_column :case_versions, :case_category_id if column_exists? :case_versions, :case_category_id
      rescue PG::UndefinedColumn => e
        p "WARNING: #{e.message}"
      end
    end
  end

  def self.down
  end
end
