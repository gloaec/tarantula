class AddDefectFieldToStepExecution < ActiveRecord::Migration[4.2]
  def self.up
    add_column :step_executions, :defect, :text
  end

  def self.down
    remove_column :step_executions, :defect
  end
end
