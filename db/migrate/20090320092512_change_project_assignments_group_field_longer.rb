class ChangeProjectAssignmentsGroupFieldLonger < ActiveRecord::Migration[4.2]
  def self.up
    change_column :project_assignments, :group, :string
  end

  def self.down
  end
end
