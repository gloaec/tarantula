p = Project.find_or_create_by!(name: 'Toolib')
u = User.where(login: "admin").first_or_create!(
  password: 'admin', password_confirmation: 'admin', email: 'admin@toolib.fr'
)
ProjectAssignment.first_or_create!(
  project: p,
  user: u,
  group: 'MANAGER'
)
