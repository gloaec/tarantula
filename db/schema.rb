# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2012_09_05_062740) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attachment_sets", id: :serial, force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "host_id"
    t.string "host_type"
    t.integer "host_version"
  end

  create_table "attachment_sets_attachments", id: false, force: :cascade do |t|
    t.integer "attachment_set_id"
    t.integer "attachment_id"
  end

  create_table "attachments", id: :serial, force: :cascade do |t|
    t.string "orig_filename"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "type", default: "Attachment"
    t.text "data"
  end

  create_table "bug_components", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "external_id"
    t.integer "bug_product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["bug_product_id"], name: "index_bug_components_on_bug_product_id"
    t.index ["external_id"], name: "index_bug_components_on_external_id"
  end

  create_table "bug_products", id: :serial, force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "name"
    t.integer "bug_tracker_id"
    t.string "external_id"
    t.index ["bug_tracker_id"], name: "index_bug_products_on_bug_tracker_id"
    t.index ["external_id"], name: "index_bug_products_on_external_id"
  end

  create_table "bug_products_projects", id: false, force: :cascade do |t|
    t.integer "bug_product_id"
    t.integer "project_id"
    t.index ["bug_product_id", "project_id"], name: "index_bug_products_projects_on_bug_product_id_and_project_id"
  end

  create_table "bug_products_test_areas", id: false, force: :cascade do |t|
    t.integer "bug_product_id"
    t.integer "test_area_id"
    t.index ["bug_product_id", "test_area_id"], name: "bug_product_test_area_idx"
  end

  create_table "bug_severities", id: :serial, force: :cascade do |t|
    t.integer "bug_tracker_id"
    t.string "name"
    t.string "sortkey"
    t.string "external_id"
    t.index ["bug_tracker_id"], name: "index_bug_severities_on_bug_tracker_id"
    t.index ["external_id"], name: "index_bug_severities_on_external_id"
    t.index ["sortkey"], name: "index_bug_severities_on_sortkey"
  end

  create_table "bug_snapshots", id: :serial, force: :cascade do |t|
    t.integer "bug_id"
    t.integer "bug_tracker_snapshot_id"
    t.integer "bug_component_id"
    t.integer "bug_product_id"
    t.integer "bug_severity_id"
    t.integer "created_by"
    t.string "external_id"
    t.string "priority"
    t.boolean "reported_via_tarantula"
    t.string "status"
    t.string "summary"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.time "lastdiffed"
    t.index ["bug_component_id"], name: "index_bug_snapshots_on_bug_component_id"
    t.index ["bug_id"], name: "index_bug_snapshots_on_bug_id"
    t.index ["bug_product_id"], name: "index_bug_snapshots_on_bug_product_id"
    t.index ["bug_severity_id"], name: "index_bug_snapshots_on_bug_severity_id"
    t.index ["bug_tracker_snapshot_id"], name: "index_bug_snapshots_on_bug_tracker_snapshot_id"
    t.index ["external_id"], name: "index_bug_snapshots_on_external_id"
    t.index ["lastdiffed"], name: "index_bug_snapshots_on_lastdiffed"
    t.index ["priority"], name: "index_bug_snapshots_on_priority"
    t.index ["status"], name: "index_bug_snapshots_on_status"
  end

  create_table "bug_tracker_snapshots", id: :serial, force: :cascade do |t|
    t.integer "bug_tracker_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "name"
    t.index ["bug_tracker_id"], name: "index_bug_tracker_snapshots_on_bug_tracker_id"
  end

  create_table "bug_trackers", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "base_url"
    t.string "db_host"
    t.string "db_port"
    t.string "db_name"
    t.string "db_user"
    t.string "db_passwd"
    t.datetime "last_fetched", default: "1900-01-01 00:00:00"
    t.string "type", default: "Bugzilla"
    t.integer "import_source_id"
    t.boolean "sync_project_with_classification", default: false
  end

  create_table "bugs", id: :serial, force: :cascade do |t|
    t.integer "bug_tracker_id"
    t.integer "bug_severity_id"
    t.string "external_id"
    t.string "summary"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "bug_product_id"
    t.integer "bug_component_id"
    t.string "status"
    t.integer "created_by"
    t.string "priority"
    t.boolean "reported_via_tarantula", default: false
    t.time "lastdiffed"
    t.string "url"
    t.index ["bug_component_id"], name: "index_bugs_on_bug_component_id"
    t.index ["bug_product_id"], name: "index_bugs_on_bug_product_id"
    t.index ["bug_severity_id"], name: "index_bugs_on_bug_severity_id"
    t.index ["bug_tracker_id"], name: "index_bugs_on_bug_tracker_id"
    t.index ["external_id"], name: "index_bugs_on_external_id"
    t.index ["lastdiffed"], name: "index_bugs_on_lastdiffed"
    t.index ["priority"], name: "index_bugs_on_priority"
    t.index ["status"], name: "index_bugs_on_status"
  end

  create_table "case_executions", id: :serial, force: :cascade do |t|
    t.integer "case_id"
    t.string "result"
    t.datetime "created_at"
    t.integer "created_by"
    t.integer "execution_id"
    t.integer "case_version"
    t.integer "assigned_to"
    t.datetime "executed_at"
    t.integer "executed_by"
    t.integer "duration", default: 0
    t.integer "position", default: 0
    t.string "title"
    t.index ["case_id"], name: "index_case_executions_on_case_id"
    t.index ["case_version"], name: "index_case_executions_on_case_version"
    t.index ["executed_by"], name: "index_case_executions_on_executed_by"
    t.index ["execution_id"], name: "index_case_executions_on_execution_id"
    t.index ["result"], name: "index_case_executions_on_result"
  end

  create_table "case_versions", force: :cascade do |t|
    t.integer "case_id"
    t.integer "version"
    t.string "title"
    t.integer "created_by"
    t.datetime "created_at"
    t.integer "updated_by"
    t.datetime "updated_at"
    t.text "objective"
    t.text "test_data"
    t.text "preconditions_and_assumptions"
    t.integer "time_estimate"
    t.integer "project_id"
    t.boolean "deleted", default: false
    t.integer "original_id"
    t.string "change_comment", default: ""
    t.string "external_id"
    t.date "date"
    t.integer "priority", default: 0
    t.boolean "archived", default: false
    t.index ["case_id"], name: "index_case_versions_on_case_id"
    t.index ["version"], name: "index_case_versions_on_version"
  end

  create_table "cases", id: :serial, force: :cascade do |t|
    t.string "title"
    t.integer "created_by"
    t.datetime "created_at"
    t.integer "updated_by"
    t.datetime "updated_at"
    t.text "objective"
    t.text "test_data"
    t.text "preconditions_and_assumptions"
    t.integer "time_estimate"
    t.integer "project_id"
    t.integer "version", default: 1
    t.boolean "deleted", default: false
    t.integer "original_id"
    t.string "change_comment", default: ""
    t.string "external_id"
    t.date "date"
    t.integer "priority", default: 0
    t.boolean "archived", default: false
    t.index ["deleted"], name: "index_cases_on_deleted"
    t.index ["id"], name: "index_cases_on_id"
    t.index ["priority", "title"], name: "index_cases_on_priority_and_title"
    t.index ["project_id"], name: "index_cases_on_project_id"
  end

  create_table "cases_requirements", id: false, force: :cascade do |t|
    t.integer "case_id"
    t.integer "requirement_id"
    t.integer "case_version"
    t.integer "requirement_version"
  end

  create_table "cases_steps", id: false, force: :cascade do |t|
    t.integer "case_id"
    t.integer "case_version"
    t.integer "position"
    t.integer "step_id"
    t.integer "step_version"
    t.index ["case_id"], name: "index_cases_steps_on_case_id"
  end

  create_table "cases_test_areas", id: false, force: :cascade do |t|
    t.integer "case_id"
    t.integer "test_area_id"
  end

  create_table "cases_test_sets", id: false, force: :cascade do |t|
    t.integer "case_id", null: false
    t.integer "test_set_id", null: false
    t.integer "position", default: 0, null: false
    t.integer "version", default: 1
    t.integer "test_set_version", default: 1
    t.integer "case_version", default: 1
    t.index ["case_id"], name: "index_cases_test_sets_on_case_id"
    t.index ["case_version"], name: "index_cases_test_sets_on_case_version"
    t.index ["test_set_id"], name: "index_cases_test_sets_on_test_set_id"
    t.index ["test_set_version"], name: "index_cases_test_sets_on_test_set_version"
  end

  create_table "customer_configs", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "value"
    t.boolean "required"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", id: :serial, force: :cascade do |t|
    t.integer "priority", default: 0
    t.integer "attempts", default: 0
    t.text "handler"
    t.string "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "queue"
    t.index ["locked_by"], name: "index_delayed_jobs_on_locked_by"
  end

  create_table "executions", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "test_set_id"
    t.datetime "created_at"
    t.integer "created_by"
    t.datetime "updated_at"
    t.integer "updated_by"
    t.integer "test_set_version", default: 1
    t.boolean "deleted", default: false
    t.integer "version", default: 0
    t.integer "test_object_id"
    t.integer "project_id"
    t.boolean "completed", default: false
    t.date "date"
    t.boolean "archived", default: false
    t.index ["deleted"], name: "index_executions_on_deleted"
    t.index ["id"], name: "index_executions_on_id"
    t.index ["project_id"], name: "index_executions_on_project_id"
    t.index ["test_object_id"], name: "index_executions_on_test_object_id"
    t.index ["test_set_id"], name: "index_executions_on_test_set_id"
    t.index ["test_set_version"], name: "index_executions_on_test_set_version"
  end

  create_table "executions_test_areas", id: false, force: :cascade do |t|
    t.integer "execution_id"
    t.integer "test_area_id"
  end

  create_table "import_sources", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "adapter"
    t.string "host"
    t.string "username"
    t.string "password"
    t.string "database"
    t.integer "port"
  end

  create_table "password_resets", id: :serial, force: :cascade do |t|
    t.string "link"
    t.boolean "activated", default: false
    t.integer "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "preferences", id: :serial, force: :cascade do |t|
    t.string "type"
    t.integer "user_id"
    t.integer "project_id"
    t.text "data"
  end

  create_table "project_assignments", id: :serial, force: :cascade do |t|
    t.integer "project_id", null: false
    t.integer "user_id", null: false
    t.string "group", default: "GUEST", null: false
    t.integer "test_area_id"
    t.boolean "test_area_forced", default: false
    t.integer "test_object_id"
  end

  create_table "projects", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.boolean "deleted", default: false
    t.integer "version", default: 0
    t.boolean "library", default: false
    t.integer "bug_tracker_id"
  end

  create_table "report_data", id: :serial, force: :cascade do |t|
    t.string "key"
    t.integer "project_id"
    t.integer "user_id"
    t.text "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "requirement_versions", id: :serial, force: :cascade do |t|
    t.integer "requirement_id"
    t.integer "version"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "external_id"
    t.string "name"
    t.integer "project_id"
    t.integer "created_by"
    t.date "date"
    t.boolean "deleted"
    t.date "external_modified_on"
    t.text "description"
    t.string "priority"
    t.text "optionals"
    t.boolean "archived", default: false
  end

  create_table "requirements", id: :serial, force: :cascade do |t|
    t.string "external_id"
    t.string "name"
    t.integer "project_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "deleted", default: false
    t.integer "created_by"
    t.date "external_modified_on"
    t.text "description"
    t.string "priority"
    t.text "optionals"
    t.date "date"
    t.integer "version", default: 1
    t.boolean "archived", default: false
    t.index ["external_id"], name: "index_requirements_on_external_id"
    t.index ["name"], name: "index_requirements_on_name"
    t.index ["project_id"], name: "index_requirements_on_project_id"
  end

  create_table "requirements_test_areas", id: false, force: :cascade do |t|
    t.integer "requirement_id"
    t.integer "test_area_id"
  end

  create_table "step_executions", id: :serial, force: :cascade do |t|
    t.integer "step_id"
    t.string "result", limit: 10
    t.integer "case_execution_id"
    t.text "comment"
    t.integer "step_version"
    t.integer "position", default: 0
    t.integer "bug_id"
    t.index ["case_execution_id"], name: "index_step_executions_on_case_execution_id"
    t.index ["step_id"], name: "index_step_executions_on_step_id"
  end

  create_table "step_versions", force: :cascade do |t|
    t.integer "step_id"
    t.integer "version"
    t.integer "case_id"
    t.text "action"
    t.text "result"
    t.integer "order"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "deleted", default: false
    t.index ["case_id"], name: "index_step_versions_on_case_id"
    t.index ["step_id"], name: "index_step_versions_on_step_id"
  end

  create_table "steps", id: :serial, force: :cascade do |t|
    t.text "action"
    t.text "result"
    t.integer "version", default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "deleted", default: false
  end

  create_table "taggings", id: :serial, force: :cascade do |t|
    t.integer "tag_id", null: false
    t.integer "taggable_id", null: false
    t.string "taggable_type", null: false
    t.index ["tag_id", "taggable_id", "taggable_type"], name: "index_taggings_on_tag_id_and_taggable_id_and_taggable_type", unique: true
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
  end

  create_table "tags", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.integer "project_id"
    t.string "taggable_type"
    t.index ["id"], name: "index_tags_on_id"
    t.index ["name"], name: "index_tags_on_name"
  end

  create_table "tasks", id: :serial, force: :cascade do |t|
    t.string "type"
    t.integer "resource_id"
    t.string "resource_type"
    t.text "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "assigned_to"
    t.boolean "finished", default: false
    t.integer "project_id"
    t.integer "created_by"
  end

  create_table "test_areas", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "project_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "test_areas_test_objects", id: false, force: :cascade do |t|
    t.integer "test_object_id"
    t.integer "test_area_id"
  end

  create_table "test_areas_test_sets", id: false, force: :cascade do |t|
    t.integer "test_set_id"
    t.integer "test_area_id"
  end

  create_table "test_objects", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "project_id"
    t.date "date"
    t.string "esw"
    t.string "swa"
    t.string "hardware"
    t.string "mechanics"
    t.text "description"
    t.boolean "deleted", default: false
    t.boolean "archived", default: false
    t.index ["created_at"], name: "index_test_objects_on_created_at"
    t.index ["date"], name: "index_test_objects_on_date"
    t.index ["name"], name: "index_test_objects_on_name"
    t.index ["project_id"], name: "index_test_objects_on_project_id"
  end

  create_table "test_set_versions", force: :cascade do |t|
    t.integer "test_set_id"
    t.integer "version"
    t.string "name"
    t.datetime "created_at"
    t.integer "created_by"
    t.datetime "updated_at"
    t.integer "updated_by"
    t.integer "project_id"
    t.boolean "deleted", default: false
    t.integer "priority", default: 0
    t.string "external_id"
    t.date "date"
    t.boolean "archived", default: false
    t.index ["test_set_id"], name: "index_test_set_versions_on_test_set_id"
  end

  create_table "test_sets", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at"
    t.integer "created_by"
    t.datetime "updated_at"
    t.integer "updated_by"
    t.integer "project_id"
    t.integer "version", default: 1
    t.boolean "deleted", default: false
    t.integer "priority", default: 0
    t.string "external_id"
    t.date "date"
    t.boolean "archived", default: false
    t.index ["priority", "name"], name: "index_test_sets_on_priority_and_name"
    t.index ["project_id"], name: "index_test_sets_on_project_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "login"
    t.string "email"
    t.string "crypted_password", limit: 40
    t.string "salt", limit: 40
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "remember_token"
    t.datetime "remember_token_expires_at"
    t.string "phone"
    t.string "realname"
    t.text "description"
    t.integer "latest_project_id"
    t.string "time_zone"
    t.boolean "deleted", default: false
    t.integer "version", default: 0
    t.string "type", default: "User"
    t.string "md5_password"
  end

end
