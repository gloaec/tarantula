=begin rdoc

Include this module to models which need attachments.

Note: You should use #attach and #unattach, not #attachments << ... 
      or the like.

=end
module AttachingExtensions
  
  def self.included(host)
    # NOTE: Throw errors en requirements not defined column
    version_present = (host.table_exists? && host.try(:column_names) || []).include?('version') rescue false
    host.instance_eval { define_method(:version) {0} } unless version_present

    # NOTE: Commented  unsupported methods
    host.has_many :all_attachment_sets,
                  -> { self.try(:version) ? where("host_version <= #{self.version}").order(id: :desc) : order(id: :desc) },
                  :class_name => 'AttachmentSet',
                  :as => :host# :order => 'id DESC',
        #:conditions => proc {"host_version <= #{self.version}"}

    host.has_one :attachment_set,
        -> { self.try(:version) ? where("host_version <= #{self.version}").order(id: :desc) : order(id: :desc) },
        :as => :host# :order => 'id DESC',
        #:conditions => proc {"host_version <= #{self.version}"}
    
    host.delegate :attachments, :to => 'self.attachment_set or return []'
  end
  
  def attach(att)
    raise "Expected Attachment, not #{att.class}" unless att.is_a? Attachment
    
    # close in transaction getting the current set and creating a new
    self.class.transaction do
      lock!
      AttachmentSet.transaction do
        set = self.all_attachment_sets.first
        # NOTE: Removed lock on set
        return false if set and set.attachments.include?(att)
      
        new_attachments = set ? set.attachments : []
        new_attachments += [att]
        AttachmentSet.create!(:host_id => self.id,
                              :host_type => self.class.to_s,
                              :host_version => self.version,
                              :attachments => new_attachments)
      end
      
      self.send(:clear_association_cache)
      att
    end
  end
  
  def unattach(att)    
    # close in transaction getting the current set and creating a new
    self.class.transaction do 
      lock!
      AttachmentSet.transaction do
        set = self.all_attachment_sets.find(:first, :lock => true)
        return false if !set or !set.attachments.include?(att)
      
        new_attachments = set.attachments - [att]
      
        AttachmentSet.create!(:host_id => self.id,
                              :host_type => self.class.to_s,
                              :host_version => self.version,
                              :attachments => new_attachments)
        end
    
        self.send(:clear_association_cache)
        att
      end
    end
  
  private
  
end
